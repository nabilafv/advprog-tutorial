package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class AuthenticWoodFiredCrustDough implements Dough {
    public String toString() {
        return "Authentic Wood Fired Crust Dough";
    }
}
