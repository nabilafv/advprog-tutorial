package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class RanchSauce implements Sauce {
    public String toString() {
        return "Ranch sauce";
    }
}
