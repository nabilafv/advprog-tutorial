package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SmokedPacificRazorClams implements Clams {

    public String toString() {
        return "Smoked Pacific Razor Clams from the eastern Aleutian Islands";
    }
}
