package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class CheeseTest {
    private Class<?> cheese;

    @Before
    public void setUp() throws Exception {
        cheese = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public void testCheeseIsAPublicInterface() {
        int classModifiers = cheese.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testCheeseHasToStringAbstractMethod() throws Exception {
        Method toString = cheese.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
