package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RedPepperTest {

    private RedPepper redPepper;

    @Before
    public void setUp() throws Exception {
        redPepper = new RedPepper();
    }

    @Test
    public void testRedPepperIsAVeggies() {
        assertNotNull(redPepper);
        assertTrue(redPepper instanceof Veggies);
    }

    @Test
    public void testRedPepperOverrideToStringMethod() throws Exception {
        Method toString = redPepper.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testRedPepperToStringMethod() {
        assertEquals(redPepper.toString(), "Red Pepper");
    }

}
