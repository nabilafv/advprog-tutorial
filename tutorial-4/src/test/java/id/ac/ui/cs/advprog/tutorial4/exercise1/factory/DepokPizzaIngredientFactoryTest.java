package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SmokedPacificRazorClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.AuthenticWoodFiredCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RanchSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DepokPizzaIngredientFactoryTest {

    private PizzaIngredientFactory depokFactory = new DepokPizzaIngredientFactory();

    @Test
    public void testCreateDough() {
        Dough dough = depokFactory.createDough();
        assertNotNull(dough);
        assertTrue(dough instanceof AuthenticWoodFiredCrustDough);
    }

    @Test
    public void testCreateSauce() {
        Sauce sauce = depokFactory.createSauce();
        assertNotNull(sauce);
        assertTrue(sauce instanceof RanchSauce);
    }

    @Test
    public void testCreateCheese() {
        Cheese cheese = depokFactory.createCheese();
        assertNotNull(cheese);
        assertTrue(cheese instanceof GoudaCheese);
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = depokFactory.createVeggies();
        assertNotNull(veggies);
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    assertTrue(veggies[i] instanceof BlackOlives);
                    break;
                case 1:
                    assertTrue(veggies[i] instanceof Broccoli);
                    break;
                case 2:
                    assertTrue(veggies[i] instanceof Eggplant);
                    break;
                case 3:
                    assertTrue(veggies[i] instanceof Spinach);
                    break;
            }
        }
    }

    @Test
    public void testCreateClam() {
        Clams clams = depokFactory.createClam();
        assertNotNull(clams);
        assertTrue(clams instanceof SmokedPacificRazorClams);
    }
}
