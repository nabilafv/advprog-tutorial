package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThickCrustDoughTest {

    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() throws Exception {
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void testThickCrustDoughIsADough() {
        assertNotNull(thickCrustDough);
        assertTrue(thickCrustDough instanceof Dough);
    }

    @Test
    public void testThickCrustDoughOverrideToStringMethod() throws Exception {
        Method toString = thickCrustDough.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testThickCrustDoughToStringMethod() {
        assertEquals(thickCrustDough.toString(), "ThickCrust style extra thick crust dough");
    }

}
