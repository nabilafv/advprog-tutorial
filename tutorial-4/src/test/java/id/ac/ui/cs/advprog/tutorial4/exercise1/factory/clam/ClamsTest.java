package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class ClamsTest {
    private Class<?> clams;

    @Before
    public void setUp() throws Exception {
        clams = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = clams.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testClamsHasToStringAbstractMethod() throws Exception {
        Method toString = clams.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
