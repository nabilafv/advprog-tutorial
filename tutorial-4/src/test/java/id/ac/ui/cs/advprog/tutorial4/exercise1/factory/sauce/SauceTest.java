package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class SauceTest {
    private Class<?> sauce;

    @Before
    public void setUp() throws Exception {
        sauce = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
    }

    @Test
    public void testSauceIsAPublicInterface() {
        int classModifiers = sauce.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSauceHasToStringAbstractMethod() throws Exception {
        Method toString = sauce.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
