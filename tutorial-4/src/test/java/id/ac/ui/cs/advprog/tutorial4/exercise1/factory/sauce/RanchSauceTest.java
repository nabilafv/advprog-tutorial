package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RanchSauceTest {

    private RanchSauce ranchSauce;

    @Before
    public void setUp() throws Exception {
        ranchSauce = new RanchSauce();
    }

    @Test
    public void testRanchSauceIsASauce() {
        assertNotNull(ranchSauce);
        assertTrue(ranchSauce instanceof Sauce);
    }

    @Test
    public void testRanchSauceOverrideToStringMethod() throws Exception {
        Method toString = ranchSauce.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testRanchSauceToStringMethod() {
        assertEquals(ranchSauce.toString(), "Ranch sauce");
    }

}
