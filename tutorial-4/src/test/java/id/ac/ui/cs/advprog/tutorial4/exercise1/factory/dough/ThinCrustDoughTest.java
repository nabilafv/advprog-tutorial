package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThinCrustDoughTest {

    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() throws Exception {
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testThinCrustDoughIsADough() {
        assertNotNull(thinCrustDough);
        assertTrue(thinCrustDough instanceof Dough);
    }

    @Test
    public void testThinCrustDoughOverrideToStringMethod() throws Exception {
        Method toString = thinCrustDough.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testThinCrustDoughToStringMethod() {
        assertEquals(thinCrustDough.toString(), "Thin Crust Dough");
    }

}
