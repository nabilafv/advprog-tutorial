package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AuthenticWoodFiredCrustDoughTest {

    private AuthenticWoodFiredCrustDough authenticWoodFiredCrustDough;

    @Before
    public void setUp() throws Exception {
        authenticWoodFiredCrustDough = new AuthenticWoodFiredCrustDough();
    }

    @Test
    public void testAuthenticWoodFiredCrustDoughIsADough() {
        assertNotNull(authenticWoodFiredCrustDough);
        assertTrue(authenticWoodFiredCrustDough instanceof Dough);
    }

    @Test
    public void testAuthenticWoodFiredCrustDoughOverrideToStringMethod() throws Exception {
        Method toString = authenticWoodFiredCrustDough.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testAuthenticWoodFiredCrustDoughToStringMethod() {
        assertEquals(authenticWoodFiredCrustDough.toString(), "Authentic Wood Fired Crust Dough");
    }

}
