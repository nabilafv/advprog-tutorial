package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class PizzaIngredientFactoryTest {

    private Class<?> pizzaFactory;

    @Before
    public void setUp() throws Exception {
        pizzaFactory = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory");
    }

    @Test
    public void testPizzaIngredientFactoryIsAPublicInterface() {
        int classModifiers = pizzaFactory.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateDoughAbstractMethod() throws Exception {
        Method toString = pizzaFactory.getDeclaredMethod("createDough");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateSauceAbstractMethod() throws Exception {
        Method toString = pizzaFactory.getDeclaredMethod("createSauce");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateCheeseAbstractMethod() throws Exception {
        Method toString = pizzaFactory.getDeclaredMethod("createCheese");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateVeggiesAbstractMethod() throws Exception {
        Method toString = pizzaFactory.getDeclaredMethod("createVeggies");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateClamAbstractMethod() throws Exception {
        Method toString = pizzaFactory.getDeclaredMethod("createClam");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
