package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class DoughTest {
    private Class<?> dough;

    @Before
    public void setUp() throws Exception {
        dough = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = dough.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testDoughHasToStringAbstractMethod() throws Exception {
        Method toString = dough.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
