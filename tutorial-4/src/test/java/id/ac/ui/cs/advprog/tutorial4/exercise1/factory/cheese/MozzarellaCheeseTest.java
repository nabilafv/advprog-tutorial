package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MozzarellaCheeseTest {

    private MozzarellaCheese mozzarellaCheese;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void testMozzarellaCheeseIsACheese() {
        assertNotNull(mozzarellaCheese);
        assertTrue(mozzarellaCheese instanceof Cheese);
    }

    @Test
    public void testMozzarellaCheeseOverrideToStringMethod() throws Exception {
        Method toString = mozzarellaCheese.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMozzarellaCheeseToStringMethod() {
        assertEquals(mozzarellaCheese.toString(), "Shredded Mozzarella");
    }

}
