package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BlackOlivesTest {

    private BlackOlives blackOlives;

    @Before
    public void setUp() throws Exception {
        blackOlives = new BlackOlives();
    }

    @Test
    public void testBlackOlivesIsAVeggies() {
        assertNotNull(blackOlives);
        assertTrue(blackOlives instanceof Veggies);
    }

    @Test
    public void testBlackOlivesOverrideToStringMethod() throws Exception {
        Method toString = blackOlives.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testBlackOlivesToStringMethod() {
        assertEquals(blackOlives.toString(), "Black Olives");
    }

}
