package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OnionTest {

    private Onion onion;

    @Before
    public void setUp() throws Exception {
        onion = new Onion();
    }

    @Test
    public void testOnionIsAVeggies() {
        assertNotNull(onion);
        assertTrue(onion instanceof Veggies);
    }

    @Test
    public void testOnionOverrideToStringMethod() throws Exception {
        Method toString = onion.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOnionToStringMethod() {
        assertEquals(onion.toString(), "Onion");
    }

}
