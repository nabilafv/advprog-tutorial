package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlumTomatoSauceTest {

    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testPlumTomatoSauceIsASauce() {
        assertNotNull(plumTomatoSauce);
        assertTrue(plumTomatoSauce instanceof Sauce);
    }

    @Test
    public void testPlumTomatoSauceOverrideToStringMethod() throws Exception {
        Method toString = plumTomatoSauce.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPlumTomatoSauceToStringMethod() {
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
    }

}
