package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MushroomTest {

    private Mushroom mushroom;

    @Before
    public void setUp() throws Exception {
        mushroom = new Mushroom();
    }

    @Test
    public void testMushroomIsAVeggies() {
        assertNotNull(mushroom);
        assertTrue(mushroom instanceof Veggies);
    }

    @Test
    public void testMushroomOverrideToStringMethod() throws Exception {
        Method toString = mushroom.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMushroomToStringMethod() {
        assertEquals(mushroom.toString(), "Mushrooms");
    }

}
