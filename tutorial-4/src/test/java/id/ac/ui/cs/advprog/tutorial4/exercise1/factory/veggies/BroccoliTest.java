package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BroccoliTest {

    private Broccoli broccoli;

    @Before
    public void setUp() throws Exception {
        broccoli = new Broccoli();
    }

    @Test
    public void testBroccoliIsAVeggies() {
        assertNotNull(broccoli);
        assertTrue(broccoli instanceof Veggies);
    }

    @Test
    public void testBroccoliOverrideToStringMethod() throws Exception {
        Method toString = broccoli.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testBroccoliToStringMethod() {
        assertEquals(broccoli.toString(), "Broccoli");
    }

}
