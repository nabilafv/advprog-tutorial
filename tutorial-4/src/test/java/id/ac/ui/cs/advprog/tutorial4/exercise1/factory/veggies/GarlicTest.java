package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GarlicTest {

    private Garlic garlic;

    @Before
    public void setUp() throws Exception {
        garlic = new Garlic();
    }

    @Test
    public void testGarlicIsAVeggies() {
        assertNotNull(garlic);
        assertTrue(garlic instanceof Veggies);
    }

    @Test
    public void testGarlicOverrideToStringMethod() throws Exception {
        Method toString = garlic.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGarlicToStringMethod() {
        assertEquals(garlic.toString(), "Garlic");
    }

}
