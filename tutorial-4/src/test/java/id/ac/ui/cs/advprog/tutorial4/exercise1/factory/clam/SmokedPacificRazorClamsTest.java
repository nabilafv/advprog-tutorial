package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SmokedPacificRazorClamsTest {

    private SmokedPacificRazorClams smokedPacificRazorClams;

    @Before
    public void setUp() throws Exception {
        smokedPacificRazorClams = new SmokedPacificRazorClams();
    }

    @Test
    public void testSmokedPacificRazorClamsIsAClams() {
        assertNotNull(smokedPacificRazorClams);
        assertTrue(smokedPacificRazorClams instanceof Clams);
    }

    @Test
    public void testSmokedPacificRazorClamsOverrideToStringMethod() throws Exception {
        Method toString = smokedPacificRazorClams.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testSmokedPacificRazorClamsToStringMethod() {
        assertEquals(smokedPacificRazorClams.toString(), "Smoked Pacific Razor Clams from the eastern Aleutian Islands");
    }

}
