package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

public class NewYorkPizzaIngredientFactoryTest {

    private PizzaIngredientFactory nyFactory = new NewYorkPizzaIngredientFactory();

    @Test
    public void testCreateDough() {
        Dough dough = nyFactory.createDough();
        assertNotNull(dough);
        assertTrue(dough instanceof ThinCrustDough);
    }

    @Test
    public void testCreateSauce() {
        Sauce sauce = nyFactory.createSauce();
        assertNotNull(sauce);
        assertTrue(sauce instanceof MarinaraSauce);
    }

    @Test
    public void testCreateCheese() {
        Cheese cheese = nyFactory.createCheese();
        assertNotNull(cheese);
        assertTrue(cheese instanceof ReggianoCheese);
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] veggies = nyFactory.createVeggies();
        assertNotNull(veggies);
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    assertTrue(veggies[i] instanceof Garlic);
                    break;
                case 1:
                    assertTrue(veggies[i] instanceof Onion);
                    break;
                case 2:
                    assertTrue(veggies[i] instanceof Mushroom);
                    break;
                case 3:
                    assertTrue(veggies[i] instanceof RedPepper);
                    break;
            }
        }
    }

    @Test
    public void testCreateClam() {
        Clams clams = nyFactory.createClam();
        assertNotNull(clams);
        assertTrue(clams instanceof FreshClams);
    }
}
