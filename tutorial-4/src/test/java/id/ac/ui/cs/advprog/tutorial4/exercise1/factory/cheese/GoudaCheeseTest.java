package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GoudaCheeseTest {

    private GoudaCheese goudaCheese;

    @Before
    public void setUp() throws Exception {
        goudaCheese = new GoudaCheese();
    }

    @Test
    public void testGoudaCheeseIsACheese() {
        assertNotNull(goudaCheese);
        assertTrue(goudaCheese instanceof Cheese);
    }

    @Test
    public void testGoudaCheeseOverrideToStringMethod() throws Exception {
        Method toString = goudaCheese.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testGoudaCheeseToStringMethod() {
        assertEquals(goudaCheese.toString(), "Gouda Cheese");
    }

}
