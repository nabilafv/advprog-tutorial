package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FrozenClamsTest {

    private FrozenClams frozenClams;

    @Before
    public void setUp() throws Exception {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testFrozenClamsIsAClams() {
        assertNotNull(frozenClams);
        assertTrue(frozenClams instanceof Clams);
    }

    @Test
    public void testFrozenClamsOverrideToStringMethod() throws Exception {
        Method toString = frozenClams.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testFrozenClamsToStringMethod() {
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
    }

}
