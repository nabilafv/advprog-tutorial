package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpinachTest {

    private Spinach spinach;

    @Before
    public void setUp() throws Exception {
        spinach = new Spinach();
    }

    @Test
    public void testSpinachIsAVeggies() {
        assertNotNull(spinach);
        assertTrue(spinach instanceof Veggies);
    }

    @Test
    public void testSpinachOverrideToStringMethod() throws Exception {
        Method toString = spinach.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testSpinachToStringMethod() {
        assertEquals(spinach.toString(), "Spinach");
    }

}