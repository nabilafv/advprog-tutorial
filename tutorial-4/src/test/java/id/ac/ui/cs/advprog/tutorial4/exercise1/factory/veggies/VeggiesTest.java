package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class VeggiesTest {
    private Class<?> veggies;

    @Before
    public void setUp() throws Exception {
        veggies = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testVeggiesIsAPublicInterface() {
        int classModifiers = veggies.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testVeggiesHasToStringAbstractMethod() throws Exception {
        Method toString = veggies.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
