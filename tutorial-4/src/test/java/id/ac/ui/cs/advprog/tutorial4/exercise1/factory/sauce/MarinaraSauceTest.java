package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MarinaraSauceTest {

    private MarinaraSauce marinaraSauce;

    @Before
    public void setUp() throws Exception {
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    public void testMarinaraSauceIsASauce() {
        assertNotNull(marinaraSauce);
        assertTrue(marinaraSauce instanceof Sauce);
    }

    @Test
    public void testMarinaraSauceOverrideToStringMethod() throws Exception {
        Method toString = marinaraSauce.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMarinaraSauceToStringMethod() {
        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
    }

}
