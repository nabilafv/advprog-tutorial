package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParmesanCheeseTest {

    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() throws Exception {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testParmesanCheeseIsACheese() {
        assertNotNull(parmesanCheese);
        assertTrue(parmesanCheese instanceof Cheese);
    }

    @Test
    public void testParmesanCheeseOverrideToStringMethod() throws Exception {
        Method toString = parmesanCheese.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testParmesanCheeseToStringMethod() {
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");
    }

}
