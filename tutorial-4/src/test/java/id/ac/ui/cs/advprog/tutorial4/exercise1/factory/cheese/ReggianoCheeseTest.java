package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ReggianoCheeseTest {

    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() throws Exception {
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    public void testReggianoCheeseIsACheese() {
        assertNotNull(reggianoCheese);
        assertTrue(reggianoCheese instanceof Cheese);
    }

    @Test
    public void testReggianoCheeseOverrideToStringMethod() throws Exception {
        Method toString = reggianoCheese.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testReggianoCheeseToStringMethod() {
        assertEquals(reggianoCheese.toString(), "Reggiano Cheese");
    }

}
