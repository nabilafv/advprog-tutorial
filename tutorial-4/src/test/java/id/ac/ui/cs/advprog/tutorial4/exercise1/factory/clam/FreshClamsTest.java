package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FreshClamsTest {

    private FreshClams freshClams;

    @Before
    public void setUp() throws Exception {
        freshClams = new FreshClams();
    }

    @Test
    public void testFreshClamsIsAClams() {
        assertNotNull(freshClams);
        assertTrue(freshClams instanceof Clams);
    }

    @Test
    public void testFreshClamsOverrideToStringMethod() throws Exception {
        Method toString = freshClams.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testFreshClamsToStringMethod() {
        assertEquals(freshClams.toString(), "Fresh Clams from Long Island Sound");
    }

}
