package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EggplantTest {

    private Eggplant eggplant;

    @Before
    public void setUp() throws Exception {
        eggplant = new Eggplant();
    }

    @Test
    public void testEggplantIsAVeggies() {
        assertNotNull(eggplant);
        assertTrue(eggplant instanceof Veggies);
    }

    @Test
    public void testEggplantOverrideToStringMethod() throws Exception {
        Method toString = eggplant.getClass().getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testEggplantToStringMethod() {
        assertEquals(eggplant.toString(), "Eggplant");
    }

}
