package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {

    public void fly() {
        System.out.println("I believe I can't fly.\nI believe I can't touch the sky.");
    }
}
