package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class MultipleFilling {
    public static void main(String[] args) {
        //Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());

        System.out.println("---------------------------------");

        //Thin Bun Burger with Tomato, Lettuce, Cucumber
        Food thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        System.out.println(thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription());

        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);
        System.out.println(thinBunBurgerVegetarian.getDescription());

        System.out.println("---------------------------------");

        //Crusty Sandiwich with Beef Meat, Chicken Meat, Using Tomato and Chili Sauce
        Food doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);
        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription());

        System.out.println("---------------------------------");

        //No Crust Sandiwich with All Filling
        Food noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());

        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);
        System.out.println(noCrustAllFillingSandwich.getDescription());
    }
}
