package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ResumeController.class)
public class ResumeTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void visitorResume() throws Exception {
        mockMvc.perform(get("/resume").param("visitor", ""))
            .andExpect(content().string(containsString("This is my CV")));

        mockMvc.perform(get("/resume").param("visitor", "Lulz"))
            .andExpect(content().string(containsString("Lulz, I hope you interested to hire me")));
    }

    @Test(expected = AssertionError.class)
    public void visitorResumeError() throws Exception {
        mockMvc.perform(get("/resume").param("visitor", ""))
            .andExpect(content().string(containsString("Lulz, I hope you interested to hire me")));

        mockMvc.perform(get("/resume").param("visitor", "Lulz"))
            .andExpect(content().string(containsString("This is my CV")));
    }

}
