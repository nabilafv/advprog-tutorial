$(document).ready(
    function () {
        $("#accordion").accordion({
            header: "h3",
            active: false,
            collapsible: true,
            heightStyle: "content",
            navigation: true
        });
    });
